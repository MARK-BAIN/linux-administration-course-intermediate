# Linux administration intermediate course

This project was created to serve as educational materials for __Linux administration__ courses organized inclass, online or in any other form, under a proprietary license.
> © Copyrights MrCertified sp. z o.o., Warsaw Poland

The project is currently designed for CentOS 7.9, but should work up to CentOS Stream 8

- To setup a local environment, please install OS image (https://centos.org/download/)
or use one of machines provided with your course materials - in case it's been provided.

##### Available language versions:
At the moment there is only 1 language version available: English (lang=en)


## The materials are split into 6 blocks (modules) and relevant sections:
There are 6 modules:

- [0. Introduction to Linux](./docs/en/0-introduction-to-linux)

- [1. Storage Administration (LVM, RAID, backups)](./docs/en/1-storage-administration)

- [2. Directory services (LDAP)](./docs/en/2-directory-services-ldap)

- [3. Security](./docs/en/3-security)

- [4. Web services](./docs/en/4-web-services)

- [5. Networking (NAT, VPN, IPsec)](./docs/en/5-networking)

The sections look as follows:
```
docs/en/
├── 0-introduction-to-linux
│   ├── 0.00_basics.md
│   ├── 0.01_account_management.md
│   ├── 0.02_access_control_list_in_practice.md
│   ├── 0.03_process_management_in_Linux.md
│   ├── 0.04_administering_sudo_privileges.md
│   └── 0.05_crontab_administration.md
├── 1-storage-administration
│   ├── 1.01_creating_LVM_partition.md
│   ├── 1.02_configuring_RAID.md
│   └── 1.03_backup_tools.md
├── 2-directory-services-ldap
│   └── 2.01_ldap.md
├── 3-security
│   ├── 3.01_pam.md
│   ├── 3.02_firewalls.md
│   ├── 3.03_selinux.md
│   └── 3.04_ids_ips.md
├── 4-web-services
│   ├── 4.01_Apache.md
│   ├── 4.02_Squid.md
│   ├── 4.03_SSL.md
│   └── 4.04_Bootstrap_&_DHCP.md
└── 5-networking
    └── 5.01_networking.md
```


## Slides:
If you prefer you can find corresponding course materials in a form of slide decks:
- English (en)
	- [Linux administration intermediate](https://docs.google.com/presentation/d/1qXVuH5wHn9nUJgzGal4WMe9mEzFj-K-K649rrOHToVA)

Please note, that although slides are useful for lecture-style theory learning or teaching, the markdowns available in the [/docs](./docs) folder are the recommended way of learning practical concepts.