SSL
Secure Socket Layer - secures communication over a network so that only the sender and the receiver have access to the sensitive data that is contained within.

whenever we see URLs of websites starting with "https" then they attempt to use SSL communication requires usage of certificates and keys
Transport Layer Security protocol (TLS 1.0) is based on the latest SSL 3.0.

`Currently only TLS 1.3 should be used to secure communication.`

## Certificate signing

You generate a private-public key pair and submit a CSR (certificate signing request) to a Certificate Authority. The contents of the CSR will form part of the final server certificate.
The CA verifies whether the information on the certificate is correct and then signs it using its (the CA's) private key. It then returns the signed server certificate to you.
You import the signed server certificate into your server.

## Certificate signing

## Certificate authentication

## Server authentication procedure

    * Client connects to a web server secured with SSL (https), client requests that the server identifies itself
    * Server sends a copy of its SSL Certificate, including the server’s public key
    * Client checks whether the certificate is trusted: unexpired, unrevoked, valid and issued to the website that it is connecting to. 
    * If the client trusts the certificate, it creates, encrypts, and sends back a session key using the server's public key
    * Server decrypts the symmetric session key using its private key and begins an encrypted session with the client. The server and the client now encrypt all transmitted data with the session key.

Client authentication

## Client authentication procedure

    * Client connects to a web server secured with SSL (https), client requests that the server identifies itself
    * Server sends a copy of its SSL Certificate, including the server’s public key
    * Client responds by sending a copy of its own SSL Certificate to the server for verification (this is a mutual or two-way authentication).
    * Client checks whether the certificate is trusted: unexpired, unrevoked, valid and issued to the website that it is connecting to. 
    * If the client trusts the certificate, it creates, encrypts, and sends back a session key using the server's public key
    * Server decrypts the symmetric session key using its private key and begins an encrypted session with the client. The server and the client now encrypt all transmitted data with the session key.

`OpenSSL` is an Open Source toolkit implementing SSL/TLS & Cryptography. It has a command-line interface & an Application Programming Interface. There are a lot of tools using OpenSSL’s libraries to secure data or establish secure connections.

Composed of two layers:

* SSL Record Protocol, which is used for the transmission of bulk data.
* SSL Handshake Protocol, which is used to establish the secure connection for data transfer

Securing Apache Web Server with mod_ssl & OpenSSL
Generate The Root Certificate

```
openssl req -x509 -days 2922 -newkey rsa:1024 -md5 -out ca.crt -keyout ca.key -config .\openssl.cnf
```

Generate The CSR

```
openssl req -newkey rsa:1024 -out mec.csr -keyout mec.key -config .\openssl.cnf -reqexts v3_req
```

Sign the CSR

```
openssl x509 -req -in mec.csr -extfile .\openssl.cnf -extensions usr_cert -CA ca.crt -CAkey ca.key -CAcreateserial -sha1 -days 1461 -out mec.crt
```

Generate The PKCS12

``` bash
openssl pkcs12 -export -out mec.p12 -in mec.crt -inkey mec.key -certfile ca.crt
```

Modify The Apache Configuration File

The Apache Configuration File – httpd.conf

```
LoadModule    ssl_module modules/libssl.so AddModule mod_ssl.c
SSLEngine off
SSLSessionCache dbm:logs/ssl_cache SSLSessionCacheTimeout 300
Listen 80
Listen 443
```

```xml
<VirtualHost _default_:80>
<Location /admin>
Deny from all
</Location>
</VirtualHost>

<VirtualHost _default_:443>
SSLEngine on
SSLCertificateFile "conf/ssl.crt/mec.crt"
SSLCertificateKeyFile "conf/ssl.key/mec.key"
SSLCACertificateFile "conf/ssl.crt/ca.crt"
<Location /admin>
SSLVerifyClient require
SSLRequire %{SSL_CLIENT_S_DN_CN} eq "Administrator"
</Location>
</VirtualHost>

```
