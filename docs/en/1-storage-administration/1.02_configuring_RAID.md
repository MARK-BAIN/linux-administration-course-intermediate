# RAID
In computer storage, the standard RAID (Redundant Array of Independent Disks) levels comprise a basic set of RAID configurations that employ the techniques of striping, mirroring, or parity to create large reliable data stores from multiple general-purpose computer hard disk drives (HDDs).
- RAID allows information to be stored and accessed using several disks. RAID uses techniques such as disk striping (RAID Level 0), disk mirroring (RAID Level 1), and disk striping with parity (RAID Level 5) to achieve redundancy, lower latency, increased bandwidth, and maximized ability to recover from hard disk crashes.
- Enhances speed
- Increases storage capacity using a single virtual disk
- Minimizes disk failure


### Install mdadm tool
``` haskell
# tools for managing raid arrays
yum install mdadm -y

# review the manual for mdadm
man mdadm
```

### Create RAID 5
Pre-requisite:  you need to have 3 unmounted disks available for the RAID5 setup
``` haskell
# list available discs to choose from
fdisk -l | grep -i "dev/sd"

Disk /dev/sda: 2147 MB, 2147483648 bytes, 4194304 sectors
Disk /dev/sdb: 1073 MB, 1073741824 bytes, 2097152 sectors
Disk /dev/sdc: 1073 MB, 1073741824 bytes, 2097152 sectors
```
If your disks are mounted, run:
```sh
umount -a
# or
umount /dev/sda && umount /dev/sdb && umount /dev/sdc
```

Create RAID5 from `/dev/sda`, `/dev/sdb`, `/dev/sdc`
``` sh
mdadm --create /dev/md1 --level=5 --raid-devices=3 /dev/sda /dev/sdb /dev/sdc

mdadm: /dev/sda appears to contain an ext2fs file system
       size=2097152K  mtime=Thu Jan  1 00:00:00 1970
mdadm: /dev/sdb appears to contain an ext2fs file system
       size=1048576K  mtime=Fri Feb  4 18:51:50 2022
mdadm: partition table exists on /dev/sdb
mdadm: partition table exists on /dev/sdb but will be lost or
       meaningless after creating array
mdadm: /dev/sdc appears to contain an ext2fs file system
       size=1048576K  mtime=Fri Feb  4 18:51:36 2022
mdadm: partition table exists on /dev/sdc
mdadm: partition table exists on /dev/sdc but will be lost or
       meaningless after creating array
mdadm: largest drive (/dev/sda) exceeds size (1045504K) by more than 1%
Continue creating array? y   
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md1 started.
```
As you can see above, the disks may contain partitions, filesystems and may have different sizes.
The `mdadm` command, can handle all of that for us by overwriting the disks.

``` sh
# confirm creation of RAID partition
mdadm --detail /dev/md1

/dev/md1:
           Version : 1.2
     Creation Time : Sat Feb  5 09:18:20 2022
        Raid Level : raid5
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 3
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Sat Feb  5 09:18:27 2022
             State : clean 
    Active Devices : 3
   Working Devices : 3
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : 1
              UUID : f7554c04:e1803d0d:22b1744b:d6a7f7b5
            Events : 18

    Number   Major   Minor   RaidDevice State
       0       8        0        0      active sync   /dev/sda
       1       8       16        1      active sync   /dev/sdb
       3       8       32        2      active sync   /dev/sdc
```

The `/proc/mdstat` file shows a snapshot of the kernel's RAID/md state.
``` haskell
cat /proc/mdstat

Personalities : [raid6] [raid5] [raid4] 
md1 : active raid5 sdc[3] sdb[1] sda[0]
      2091008 blocks super 1.2 level 5, 512k chunk, algorithm 2 [3/3] [UUU]
      
unused devices: <none>
```
Now, let's create a filesystem on the new array
``` haskell
mkfs -t ext4 /dev/md1

mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=128 blocks, Stripe width=256 blocks
130816 inodes, 522752 blocks
26137 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8176 inodes per group
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 
```

Next, let's mount the array
```haskell
# create a folder
mkdir /mnt/raid_data

# show mount filesystem
mount -l

sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime,seclabel)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
devtmpfs on /dev type devtmpfs (rw,nosuid,seclabel,size=916784k,nr_inodes=229196,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,seclabel,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,nodev,seclabel,mode=755)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,seclabel,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
/dev/vda1 on / type xfs (rw,relatime,seclabel,attr2,inode64,noquota)
rpc_pipefs on /var/lib/nfs/rpc_pipefs type rpc_pipefs (rw,relatime)
selinuxfs on /sys/fs/selinux type selinuxfs (rw,relatime)
tmpfs on /run/user/0 type tmpfs (rw,nosuid,nodev,relatime,seclabel,size=188212k,mode=700)

# mount RAID partition to that folder 
mount /dev/md1 /mnt/raid_data

# show mount filesystem
mount -l

sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime,seclabel)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
devtmpfs on /dev type devtmpfs (rw,nosuid,seclabel,size=916784k,nr_inodes=229196,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,seclabel,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,nodev,seclabel,mode=755)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,seclabel,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
/dev/vda1 on / type xfs (rw,relatime,seclabel,attr2,inode64,noquota)
rpc_pipefs on /var/lib/nfs/rpc_pipefs type rpc_pipefs (rw,relatime)
selinuxfs on /sys/fs/selinux type selinuxfs (rw,relatime)
tmpfs on /run/user/0 type tmpfs (rw,nosuid,nodev,relatime,seclabel,size=188212k,mode=700)
/dev/md1 on /mnt/raid_data type ext4 (rw,relatime,seclabel,stripe=256,data=ordered)
```

We may want to analyze the configuration of the newly setup array
```sh
# show RAIS configuration
mdadm --detail -scan

ARRAY /dev/md1 metadata=1.2 name=1 UUID=f7554c04:e1803d0d:22b1744b:d6a7f7b5

# save RAID configuration
mdadm --detail -scan >> /etc/mdadm.conf

# verify RAID configuration
cat /etc/mdadm.conf 

ARRAY /dev/md1 metadata=1.2 name=1 UUID=f7554c04:e1803d0d:22b1744b:d6a7f7b5
```

### Restoring an array
``` haskell
cat /proc/mdstat

Personalities : [raid6] [raid5] [raid4] 
md1 : active raid5 sdc[3] sdb[1] sda[0]
      2091008 blocks super 1.2 level 5, 512k chunk, algorithm 2 [3/3] [UUU]
      
unused devices: <none>

# set disc sda as faulty
mdadm /dev/md1 -f /dev/sda 

mdadm: set /dev/sda faulty in /dev/md1

# show RAID state
mdadm --detail /dev/md1

/dev/md1:
           Version : 1.2
     Creation Time : Sat Feb  5 09:18:20 2022
        Raid Level : raid5
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 3
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Sat Feb  5 09:58:36 2022
             State : clean, degraded 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 1
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : 1
              UUID : f7554c04:e1803d0d:22b1744b:d6a7f7b5
            Events : 20

    Number   Major   Minor   RaidDevice State
       -       0        0        0      removed
       1       8       16        1      active sync   /dev/sdb
       3       8       32        2      active sync   /dev/sdc

       0       8        0        -      faulty   /dev/sda

# remove faulty disc
mdadm /dev/md1 -r /dev/sda

mdadm: hot removed /dev/sda from /dev/md1

# add new disc
mdadm /dev/md1 -a /dev/sda

mdadm: added /dev/sda

show RAID state
mdadm --detail /dev/md1

/dev/md1:
           Version : 1.2
     Creation Time : Sat Feb  5 09:18:20 2022
        Raid Level : raid5
        Array Size : 2091008 (2042.00 MiB 2141.19 MB)
     Used Dev Size : 1045504 (1021.00 MiB 1070.60 MB)
      Raid Devices : 3
     Total Devices : 3
       Persistence : Superblock is persistent

       Update Time : Sat Feb  5 10:03:05 2022
             State : clean 
    Active Devices : 3
   Working Devices : 3
    Failed Devices : 0
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : 1
              UUID : f7554c04:e1803d0d:22b1744b:d6a7f7b5
            Events : 40

    Number   Major   Minor   RaidDevice State
       4       8        0        0      active sync   /dev/sda
       1       8       16        1      active sync   /dev/sdb
       3       8       32        2      active sync   /dev/sdc



```

### Removing RAID
```haskell
# unmount /dev/md1
umount -l /dev/md1

# stop RAID
mdadm --stop /dev/md1

mdadm: stopped /dev/md1

# remove RAID 
mdadm --remove /dev/md1 
#(after stop file doesn't exist)

# show filesystems
lsblk --fs

NAME   FSTYPE            LABEL    UUID                                 MOUNTPOINT
sda    linux_raid_member 1        f7554c04-e180-3d0d-22b1-744bd6a7f7b5 
sdb    linux_raid_member 1        f7554c04-e180-3d0d-22b1-744bd6a7f7b5 
sdc    linux_raid_member 1        f7554c04-e180-3d0d-22b1-744bd6a7f7b5 
vda                                                                    
└─vda1 xfs                        6cd50e51-cfc6-40b9-9ec5-f32fa2e4ff02 /
vdb    iso9660           config-2 2022-02-04-19-19-56-00      

# restore raid after stopping
mdadm --assemble --scan
mdadm: /dev/md1 has been started with 3 drives

# show filesystem
lsblk --fs

NAME   FSTYPE            LABEL    UUID                                 MOUNTPOINT
sda    linux_raid_member 1        f7554c04-e180-3d0d-22b1-744bd6a7f7b5 
└─md1  ext4                       cb5ba5f9-cdcc-461b-a3cd-cfd224dc2383 
sdb    linux_raid_member 1        f7554c04-e180-3d0d-22b1-744bd6a7f7b5 
└─md1  ext4                       cb5ba5f9-cdcc-461b-a3cd-cfd224dc2383 
sdc    linux_raid_member 1        f7554c04-e180-3d0d-22b1-744bd6a7f7b5 
└─md1  ext4                       cb5ba5f9-cdcc-461b-a3cd-cfd224dc2383 
vda                                                                    
└─vda1 xfs                        6cd50e51-cfc6-40b9-9ec5-f32fa2e4ff02 /
vdb    iso9660           config-2 2022-02-04-19-19-56-00               
```