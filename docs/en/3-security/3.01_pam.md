# PAM (Pluggable Authentication Modules)

- dedicated library `libpam` - `/lib`
- modules in `/lib/security`
	- auth
	- account
	- session
	- password
- configuration in
	- `/etc/pam.conf`
	- files in `/etc/pam.d/` contain the PAM configuration files

To utilize PAM, a program has to be PAM aware. It means, it had to be written and compiled specifically to use PAM. In order to find out if a program is a PAM-aware one, run the following `ldd` command:
```sh
ldd /usr/sbin/sshd | grep libpam.so

libpam.so.0 => /lib64/libpam.so.0 (0x00007f54e8240000)

ldd /usr/sbin/crond | grep libpam.so

libpam.so.0 => /lib64/libpam.so.0 (0x00007f7c7a4ea000)
```
If it returns a result, then the program utilizes PAM

PAM loadable object files (the modules) are located within one of the following directories: 
- `/lib/security/`
- or `/lib64/security` 
depending on the system's architecture

### PAM flow
`/etc/pam.d/...`   - PAM service configuration file
- service-name    - filename
- module-type
- control-flag
- module-path
- arguments

The files are named with the actual application name that uses PAM. 
```sh
ls -lah /etc/pam.d/

cat /etc/pam.d/login
#%PAM-1.0
auth [user_unknown=ignore success=ok ignore=ignore default=bad] pam_securetty.so
auth       substack     system-auth
auth       include      postlogin
account    required     pam_nologin.so
account    include      system-auth
password   include      system-auth
# pam_selinux.so close should be the first session rule
session    required     pam_selinux.so close
session    required     pam_loginuid.so
session    optional     pam_console.so
# pam_selinux.so open should only be followed by sessions to be executed in the user context
session    required     pam_selinux.so open
session    required     pam_namespace.so
session    optional     pam_keyinit.so force revoke
session    include      system-auth
session    include      postlogin
-session   optional     pam_ck_connector.so
```

Service configuration files contain lists of rules written on a single line. 
You can extend rules using `\`. 
Comments are preceded with the `#` mark.

The order of arguments for every line is the following:
```haskell
type control-flag module module-arguments
```
- type: type, context or interface
	- account: responsible for account verification like does a user have access, is password expired, is account locked, etc.
	- authentication: authenticate a user and their credentials
	- password: responsible for updating user passwords and working along with authentication modules
	- session: manage activities at the beginning/end of a session
- control-flag: indicates the behavior of the PAM-API
	- requisite: failure instantly returns control to the application indicating the nature of the first module failure.
	- required: all these modules are required to succeed for libpam to return success to the application.
	- sufficient: given that all preceding modules have succeeded, the success of this module leads to an immediate and successful return to the application (failure of this module is ignored).
	- optional: the success or failure of this module is generally not recorded.
	- include and substack: include all lines of given type from the configuration file specified as an argument to this control.
- module: the absolute filename or relative pathname of the PAM.
- module-arguments: space separated list of tokens for controlling module behavior.



### PAM example - Restrict root access to SSH
Restrict root access to SSH Services using PAM
```haskell
vi /etc/pam.d/sshd
```

Add below configuration:
```haskell
auth required pam_listfile.so item=user sense=deny file=/etc/ssh/sshd.deny onerr=succeed
```
- **auth**: is the module type (or context).
- **required**: is a control-flag that means if the module is used, it must pass or the overall result will be fail, regardless of the status of other modules.
- **pam_listfile.so**: is a module which provides a way to deny or allow services based on an arbitrary file.
- **item=user**: module argument which specifies what is listed in the file and should be checked for.
- **sense=deny**: module argument which specifies action to take if found in file, if the item is NOT found in the file, then the opposite action is requested.
- **file=/etc/ssh/sshd.deny**: module argument which specifies file containing one item per line.
- **onerr=succeed**: module argument.


Add root name to denied users file
```sh
vi /etc/ssh/sshd.deny
```

Save the files and change permissions:
```sh
chmod 600 /etc/ssh/sshd.deny
```

Restart SSH deamon (first, please make sure you have access directly to the console)
```sh
systemctl restart sshd.service
```

Try to login and check the logs
```sh
tail -f /var/log/secure


Feb  8 18:06:48 amanda-server sshd[1921]: pam_listfile(sshd:auth): Refused user root for service sshd
Feb  8 18:06:50 amanda-server sshd[1921]: Failed password for root from 172.16.84.1 port 61369 ssh2
Feb  8 18:06:50 amanda-server sshd[1921]: pam_listfile(sshd:auth): Refused user root for service sshd
Feb  8 18:06:51 amanda-server sshd[1921]: Failed password for root from 172.16.84.1 port 61369 ssh2
```

For more information see:
```sh
man pam.d
# or 
man pam.conf
```