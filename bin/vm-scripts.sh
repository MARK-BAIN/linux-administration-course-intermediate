#!/bin/bash

# Basic while loop
zero=0
counter=1
remoteCommand="hostnamectl"
# remoteCommand="hostname=`hostname` && ssh-keygen -t ed25519 -C '$hostname' -f /root/.ssh/id_ed25519 -q -N '\"\"'"
# remoteCommand="cat /root/.ssh/id_ed25519.pub"

while [ $counter -le 15 ]
do
     zero=$([ $counter -lt 10 ] && echo 0 || echo "")

     hostname='root@centos-linux-tarnow-g2-1608485304791-pre-course-'$zero$counter
     ssh $hostname $remoteCommand
     ((counter++))
done
echo All done