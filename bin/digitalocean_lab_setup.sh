#!/bin/bash

# Basic while loop
zero=0
counter=1

while [ $counter -le 15 ]
do
	zero=$([ $counter -lt 10 ] && echo 0 || echo "")

	doctl compute volume create lt-$zero$counter-{1..3} --region=fra1   --size=1GiB  --fs-type=ext4  --tag=PWSZ-TARNOW,LINUX,G2
	((counter++))
done
echo Volumes created

# doctl compute volume list > ~/Projects/linux-administration-pl/bin/volumes
# doctl compute droplet list > ~/Projects/linux-administration-pl/bin/droplets

# https://docs.digitalocean.com/reference/doctl/how-to/install/
# https://docs.digitalocean.com/reference/doctl/reference/compute/droplet/create/
# https://docs.digitalocean.com/reference/doctl/reference/compute/volume/create/
